# File Mime Validator Module

## Contents of This File

 - Introduction
 - Requirements
 - Installation
 - Maintainers

## Introduction

 - It can perform a more secure and reliable check.
 - It works on the upload type file field used in any entity.
 - This module performs a server-side validation for the extension.
 - It helps in site making malicious type uploads out of the system.
 - Logs the upload if found malicious i.e not the type which is being requested.

 - For a full description of the module, visit the project page:
   <https://www.drupal.org/project/file_mime_validator>

 - To submit bug reports and feature suggestions, or to track changes:
   <https://www.drupal.org/project/issues/file_mime_validator>

This module is safe to be use on a production site.

## Requirements

This module requires nothing outside of Drupal core.

## Installation

 - Install this module as you install a contributed Drupal module.
   Visit [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Maintainers

 - Akshay Singh - <https://www.drupal.org/u/akshay-singh>

Supporting organizations:

 - TO THE NEW - <https://www.drupal.org/to-the-new>
